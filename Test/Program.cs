﻿using System;
using Bank.Data;
using Bank.Data.Services;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new BankContext())
            {
                var initializer = new DbInitializer();
                var service = new DbService();
                initializer.Initialize();
                service.RecalculateBalances();
                var cards = ctx.Cards;
                Console.WriteLine("New balances:");
                foreach(var card in cards)
                {
                    Console.WriteLine("Id: {0} balance={1}", card.Id, card.Balance);
                }
            }
            Console.ReadKey();
        }
    }
}
