﻿using System.Collections.Generic;

namespace Bank.Data.Models
{
    public class Card
    {
        public string Id { get; set; }
        public string PinCode { get; set; }
        public decimal Balance { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }

        public List<Operation> OutOperations { get; set; }
        public List<Operation> InOperations { get; set; }

        public Card()
        {
            OutOperations = new List<Operation>();
            InOperations = new List<Operation>();
        }
    }
}
