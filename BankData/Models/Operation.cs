﻿using System;

namespace Bank.Data.Models
{
    public class Operation
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }

        public DateTime OperationTime { get; set; }

        public string OutCardId { get; set; }
        public string InCardId { get; set; }
        public Card OutCard { get; set; }
        public Card InCard { get; set; }

        public Operation()
        {
            OperationTime = DateTime.Now;
        }
    }
}
