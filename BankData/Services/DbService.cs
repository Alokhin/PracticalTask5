﻿using System.Linq;
using System.Data.Entity;

namespace Bank.Data.Services
{
    public class DbService
    {
        public void RecalculateBalances()
        {
            using (var ctx = new BankContext())
            {
                var cards = ctx.Cards
                    .Include(c => c.InOperations)
                    .Include(c => c.OutOperations);
                foreach(var card in cards)
                {
                    card.Balance = card.InOperations.Sum(op => op.Amount) -
                        card.OutOperations.Sum(op => op.Amount);
                }
                ctx.SaveChanges();
            }
        }
    }
}
