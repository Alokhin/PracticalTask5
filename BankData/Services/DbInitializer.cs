﻿using Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bank.Data.Services
{
    public class DbInitializer
    {
        public void Initialize()
        {
            using (var ctx = new BankContext())
            {
                if (!ctx.Clients.Any())
                {
                    ClientInitialize();
                    CardInitialize();
                    OperationInitialize();
                }
            }
        }

        #region partial initializings
        private void ClientInitialize()
        {
            using (var ctx = new BankContext())
            {
                var clients = new List<Client>()
                {
                    new Client()
                    {
                        FirstName ="Tyrion",
                        LastName ="Lannister",
                        Phone = "+380501234567" ,
                        BirthDay = DateTime.Parse("1980-4-29"),
                        Address = new Address()
                        {
                            Country = "United States",
                            State = "California",
                            City = "Los Angeles",
                            AddressLine = "Green st, 71"
                        }
                    },
                    new Client()
                    {
                        FirstName ="Jon",
                        LastName ="Snow",
                        Phone = "+380660670102" ,
                        BirthDay = DateTime.Parse("1996-02-11"),
                        Address = new Address()
                        {
                            Country = "France",
                            City = "Paris",
                            AddressLine = "Baker st, 121"
                        }
                    },
                    new Client()
                    {
                        FirstName ="Ned",
                        LastName ="Stark",
                        Phone = "+380953332211" ,
                        BirthDay = DateTime.Parse("1966-12-21"),
                        Address = new Address()
                        {
                            Country = "Norway",
                            City = "Oslo",
                            AddressLine = "Akebergveien st, 16"
                        }
                    }
                };
                ctx.Clients.AddRange(clients);
                ctx.SaveChanges();
            }
        }
        private void CardInitialize()
        {
            using (var ctx = new BankContext())
            {
                var clients = ctx.Clients.ToList();
                var cards = new List<Card>()
                {
                    new Card()
                    {
                        Id = "1000000000000001",
                        Client = clients[0],
                        PinCode = "0001"
                    },
                    new Card()
                    {
                        Id = "1000000000000002",
                        Client = clients[0],
                        PinCode = "0002"
                    },
                    new Card()
                    {
                        Id = "1000000000000003" ,
                        Client = clients[0],
                        PinCode = "0003"
                    },
                    new Card()
                    {
                        Id = "2000000000000001" ,
                        Client = clients[1],
                        PinCode = "2222"
                    },
                    new Card()
                    {
                        Id = "3000000000000001",
                        Client = clients[2],
                        PinCode = "3333"
                    },
                    new Card()
                    {
                        Id = "3000000000000002",
                        Client = clients[2],
                        PinCode = "3000"
                    }
                };
                ctx.Cards.AddRange(cards);
                ctx.SaveChanges();
            }
        }
        private void OperationInitialize()
        {
            using (var ctx = new BankContext())
            {
                var operations = new List<Operation>()
                {
                    new Operation() { OutCardId = "1000000000000001", InCardId = "1000000000000002", Amount = 1000},
                    new Operation() { OutCardId = "1000000000000001", InCardId = "1000000000000003", Amount = 3000},
                    new Operation() { OutCardId = "1000000000000003", InCardId = "3000000000000001", Amount = 2500},
                    new Operation() { OutCardId = "1000000000000003", InCardId = "3000000000000002", Amount = 500},
                    new Operation() { OutCardId = "3000000000000001", InCardId = "2000000000000001", Amount = 2000},
                    new Operation() { OutCardId = "1000000000000001", InCardId = "2000000000000001", Amount = 4000},
                    new Operation() { OutCardId = "2000000000000001", InCardId = "1000000000000002", Amount = 60.0M},
                    new Operation() { OutCardId = "2000000000000001", InCardId = "1000000000000003", Amount = 51.23M},
                };
                ctx.Operations.AddRange(operations);
                ctx.SaveChanges();
            }
        }
        #endregion
    }
}
