﻿using Bank.Data.Mappings;
using Bank.Data.Models;
using System;
using System.Data.Entity;

namespace Bank.Data
{
    public class BankContext : DbContext, IDisposable
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Operation> Operations { get; set; }

        public BankContext() : base("DbConnection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            modelBuilder.Configurations.Add(new CardConfiguration());
            modelBuilder.Configurations.Add(new OperationConfiguration());
        }
    }
}
