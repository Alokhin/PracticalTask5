﻿using Bank.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bank.Data.Mappings
{
    class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("Clients");
            HasKey(c => c.Id);

            Property(c => c.Id).HasColumnName(@"ClientID").HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.FirstName).HasColumnName(@"FirstName").HasColumnType("nvarchar")
                .HasMaxLength(50).IsRequired();
            Property(c => c.LastName).HasColumnName(@"LastName").HasColumnType("nvarchar")
                .HasMaxLength(50).IsRequired();
            Property(c => c.BirthDay).HasColumnName(@"Birthday").HasColumnType("date")
                .IsRequired();
            Property(c => c.Phone).HasColumnName(@"Phone").HasColumnType("varchar")
                .HasMaxLength(20).IsRequired();

            HasOptional(c => c.Address).WithOptionalPrincipal(a => a.Client);
        }
    }
}
