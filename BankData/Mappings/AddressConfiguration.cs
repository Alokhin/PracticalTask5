﻿using Bank.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bank.Data.Mappings
{
    class AddressConfiguration : EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        {
            ToTable("Addresses");
            HasKey(a => a.Id);

            Property(a => a.Id).HasColumnName(@"ClientID").HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(a => a.Country).HasColumnName(@"Country").HasColumnType("nvarchar")
                .HasMaxLength(50).IsRequired();
            Property(a => a.State).HasColumnName(@"State").HasColumnType("nvarchar")
                .HasMaxLength(50).IsOptional();
            Property(a => a.City).HasColumnName(@"City").HasColumnType("nvarchar")
                .HasMaxLength(50).IsRequired();
            Property(a => a.AddressLine).HasColumnName(@"Address").HasColumnType("nvarchar")
                .HasMaxLength(50).IsRequired();
        }
    }
}
